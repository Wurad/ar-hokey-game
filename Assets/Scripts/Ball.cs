﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Ball : MonoBehaviour
{
    Vector3 velocity;
    [Range(0,1)]
    public float speed = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        RestBall();
    }
    void RestBall()
    {
        transform.position = Vector3.zero;
        float z = (Random.Range(0, 2) * 2) - 1;
        float x = (Random.Range(0, 2) * 2) - 1 * Random.Range(0f, 0f); //0.2f, 1f);
        velocity = new Vector3(x, 0, z);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        velocity = velocity.normalized * speed;
        transform.position += velocity;
    }
    void OnCollisionEnter(Collision collision)
    {
        switch(collision.transform.name)
        {
            case "Bounds East":
            case "Bounds West":
            case "Gate1":
            case "Gate2":
                velocity.x *= -1f;
                return;

            case "lose":
            case "lose2":
            case "but":
            case "but2":
                RestBall();
                return;

            case "player1":
            case "player2":
                velocity.z *= -1f;
                return;
        }
    }
}
